# TODO

Bon les différentes parties du projet :

* Les capteurs
    * Vent (anémomètre)
    * Girouette
    * GPS
    * Bousolle
    * Giroscope
    * Bonus :
        * Taux de salanéité
        * Baromètre
        * Température (air et eau)

* Les controleurs
    * Gouvernaille (simple servomoteur)
    * Les voiles (Servo treuil puissant)

* Mécanique
    * Coque
    * Mât
    * Voiles
    * Quille
    * gouvernaille

* Informatique
    * réglage des voiles en fonction du vent (direction et force)
    * Suivit d'itinéraire
    * élaboration de route (en prenant en compte le vent et "les obstacles")

* Énergie
    * Photovoltaïque
    * Éolien (?)
    * Batteries

* Connectivité
    * gprs/3G
    * wifi
    * HF
    * Satellite
