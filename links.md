# Links

### Competitions
* WRSC (ensta-Bretagne) : [http://www.roboticsailing.org](http://www.roboticsailing.org)



### Doc
* Prof à l'ensta Bretagne : [http://www.ensta-bretagne.fr/jaulin/](http://www.ensta-bretagne.fr/jaulin/)
* Thèse : [Autonomous Sailboat Navigation](https://www.dora.dmu.ac.uk/bitstream/handle/2086/7364/thesis-optimized-300dpi.pdf)
* Thèse : [Design and Implementation of a Navigation Algorithm for an Autonomous Sailboat](http://students.asl.ethz.ch/upl_pdf/110-report.pdf?aslsid=c77e90249dda78a4319bfedd8b990ad3)
* Thèse : [An interval approach for stability analysis; Application to sailboat robotics](https://www.ensta-bretagne.fr/jaulin/paper_checking.pdf)
* Bouquin : [Dessiner son voilier rc](http://www.velarc.it/index.php?option=com_docman&task=doc_download&gid=1)
